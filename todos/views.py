from django.shortcuts import render
from todos.models import TodoList


def todos_list_list(request):
    todos_list = TodoList.objects.all()
    context = {
        "todos_list": todos_list
    }
    return render(request, "todos/todolist.html", context)
